using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Garra.SSGE;
using Garra.Utils;
using UnityEngine;
using Object = System.Object;

namespace Garra.SimpleUISystem
{
    [RequireComponent(typeof(Transform))]
    public class UIManager : MonoBehaviour
    {
        [SerializeField] ShowPopupGameEventRequest ShowPopupRequest;
        [SerializeField] GameEvent BackButtonGameEvent;
        [SerializeField] List<GameObject> uiDatasProviderPrefabs;

        Transform _pivot;
        List<UIComponent> uiDatasProvider = new List<UIComponent>();
        List<UIComponent> _cacheDatas = new List<UIComponent>();
        UIComponent _currentUIData;

        private void OnEnable()
        {
            DontDestroyOnLoad(this);
            
            _pivot = transform;
            
            foreach (var p in uiDatasProviderPrefabs)
            {
                uiDatasProvider.Add(p.GetComponent<UIComponent>());
            }
            
            ShowPopupRequest?.AddEventListener(HandleShowPopupRequest);
            BackButtonGameEvent?.AddEventListener(HandleBackButtonEvent);
        }

        private void HandleShowPopupRequest(string id)
        {
            Show(id, () => { }, () => { });
            ShowPopupRequest.Response(_currentUIData);
        }

        private void HandleRShowPopupResponse(out UIComponent popup)
        {
            popup = _currentUIData;
        }
        
        public void Disable()
        {
            uiDatasProvider.Clear();
            BackButtonGameEvent?.RemoveEventListener(HandleBackButtonEvent);
        }

        private void HandleBackButtonEvent()
        {
            Close(_currentUIData.UiData.id);
        }

        private bool DataExist(string id, out UIComponent component)
        {
            component = uiDatasProvider.Find(x => x.UiData.id == id);
            return component != null;
        }

        private bool CacheExist(string id, out UIComponent cacheData)
        {
            cacheData = _cacheDatas.Find(x=> x.UiData.id == id);
            return cacheData != null;
        }

        private void Show(string id, Action onEnabled, Action onAnimationFinish)
        {
            if (CacheExist(id, out var cacheData))
            {
                cacheData.gameObject.SetActive(true);
                onEnabled.Invoke();
                cacheData.AnimIn(onAnimationFinish.Invoke);
                _currentUIData = cacheData;
                return;
            }
            
            if (!DataExist(id, out var data)) return;
            var clone = Instantiate(data, _pivot);
            clone.gameObject.SetActive(true);
            onEnabled.Invoke();
            clone.AnimIn(onAnimationFinish.Invoke);
            _currentUIData = clone;

            if (_cacheDatas.Contains(clone/*data*/)) return;

            clone.CloneGameObject = clone.gameObject;
            _cacheDatas.Add(clone/*data*/);
        }

        public void Close(string id)
        {
            if (!CacheExist(id, out var cacheData)) return;
        
            cacheData.AnimOut(() => 
            {
                cacheData.CloneGameObject.SetActive(false);
            });


            var cacheIdx = _cacheDatas.FindLastIndex(x => x.CloneGameObject.activeInHierarchy);
            if (cacheIdx != -1) {
                _currentUIData = _cacheDatas[cacheIdx]; 
            }
        }

        public void Delete(string id)
        {
            if (!DataExist(id, out var cacheData)) return;

            var cacheIdx = _cacheDatas.FindLastIndex(x => x.CloneGameObject.activeInHierarchy);
            if (cacheIdx != -1)
            {
                _currentUIData = _cacheDatas[cacheIdx];
            }

            if(!_currentUIData) return;
            
            _cacheDatas.Remove(_currentUIData);

            _currentUIData.AnimOut(() =>
            {
                Destroy(_currentUIData.CloneGameObject);
            });
        }
        
        private void OnDisable()
        {
            ShowPopupRequest?.RemoveEventListener(HandleShowPopupRequest);
        }
    }
}


using System;
using Garra.SSGE;
using UnityEngine;

namespace Garra.SimpleUISystem
{
    [CreateAssetMenu(fileName = "ShowPopupGameEventRequest", menuName = "MyGameEvents/ShowPopupGameEventRequest")]
    public class ShowPopupGameEventRequest : GameEventRequest<string, UIComponent>
    {
    }
}
using System;
using System.Collections;
using UnityEngine;

namespace Garra.SimpleUISystem
{
    public class UIComponent : MonoBehaviour
    {
        [SerializeField] protected UIData uiData;
        [SerializeField] protected AnimationClip outAnimation;
        [SerializeField] protected AnimationClip inAnimation;

        public UIData UiData
        {
            get
            {
                return uiData;
            }
            set
            {
                uiData = value;
            }
        }

        public GameObject CloneGameObject { get; set; }
        public string Id => uiData.id;

        protected virtual void OnEnable()
        {
            if (!uiData.animator)
            {
                return;
            }
            
            uiData.animator.SetTrigger(uiData._in);
        }

        public void AnimOut(Action onFinish)
        {
            StartCoroutine(WaitAnimation(onFinish, uiData._out, outAnimation));
        }

        IEnumerator WaitAnimation(Action onFinish, string trigger, AnimationClip animationInfo)
        {
            if (!uiData.animator)
            {
                onFinish.Invoke();
                yield break;
            }
            
            uiData.animator.SetTrigger(trigger);
            yield return new WaitForSeconds(animationInfo.length);
            onFinish.Invoke();
        }

        public void AnimIn(Action onFinish)
        {
            StartCoroutine(WaitAnimation(onFinish, uiData._in, inAnimation));
        }

        public void ResponseRequest(ref UIComponent response)
        {
            throw new NotImplementedException();
        }
    }
}
using System;
using UnityEngine;

namespace Garra.SimpleUISystem
{
    [Serializable]
    public class UIData
    {
        public string id;
        public Animator animator;
        public string _in;
        public string _out;
    }
}
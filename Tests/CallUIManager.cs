using Garra.SimpleUISystem;
using UnityEngine;
using Garra.Utils;
using static Garra.SimpleUISystem.UIManager;

public class CallUIManager : MonoBehaviour
{
    [SerializeField] ShowPopupGameEventRequest Request;
    
    public void ShowPopup()
    {
        Request.Raise("popupID");
    }
}
